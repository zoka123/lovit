<?php

namespace LovitBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LovitBundle extends Bundle
{

    public function getParent()
    {
        return 'ZantolovAppBundle';
    }
}
