<?php

namespace LovitBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Zantolov\AppBundle\Menu\MenuBuilderInterface;

class MenuBuilder implements MenuBuilderInterface
{

    private $factory;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMenu(RequestStack $requestStack)
    {
        $menuItems = array();

        $menuItems['tags'] = $this->factory->createItem('tags', array('label' => 'Tags', 'route' => 'lovit.tags.index'))
            ->setAttribute('icon', 'fa fa-tag');

        $menuItems['projects'] = $this->factory->createItem('projects', array('label' => 'Projects', 'route' => 'project.index'))
            ->setAttribute('icon', 'fa fa-list');

        $menuItems['feedbacks'] = $this->factory->createItem('feedbacks', array('label' => 'Feedbacks', 'route' => 'feedback.index'))
            ->setAttribute('icon', 'fa fa-bullhorn');

        $menuItems['groups'] = $this->factory->createItem('groups', array('label' => 'Groups', 'route' => 'groups.index'))
            ->setAttribute('icon', 'fa fa-group');

        return $menuItems;
    }


    public function getOrder()
    {
        return 0;
    }
}