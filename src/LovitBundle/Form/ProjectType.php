<?php

namespace LovitBundle\Form;

use LovitBundle\Enum\FeedbackTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zantolov\MediaBundle\Form\EventSubscriber\ImagesChooserFieldAdderSubscriber;

class ProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $imagesSubscriber = new ImagesChooserFieldAdderSubscriber('image', array('multiple' => false));
        $builder->addEventSubscriber($imagesSubscriber);

        $builder
            ->add('name')
            ->add('active', null, array('required' => false))
            ->add('description', 'ckeditor')
            ->add('allowedTypes', 'choice', array(
                'multiple' => true,
                'choices'  => FeedbackTypeEnum::getOptions()))
            ->add('tags');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LovitBundle\Entity\Project'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lovitbundle_project';
    }
}
