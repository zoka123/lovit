<?php

namespace LovitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zantolov\MediaBundle\Form\EventSubscriber\ImagesChooserFieldAdderSubscriber;

class FeedbackType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $imagesSubscriber = new ImagesChooserFieldAdderSubscriber('image', array('multiple' => false));
        $builder->addEventSubscriber($imagesSubscriber);

        $builder
            ->add('rating')
            ->add('project')
            ->add('imagePath')
            ->add('user')
            ->add('latitude')
            ->add('longitude')
            ->add('comment');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LovitBundle\Entity\Feedback'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lovitbundle_feedback';
    }
}
