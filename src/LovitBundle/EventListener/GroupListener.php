<?php

namespace LovitBundle\EventListener;

use Monolog\Logger;
use Proxies\__CG__\LovitBundle\Entity\Project;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Service\Notifications;
use Zantolov\AppBundle\Entity\User;

class GroupListener implements \Symfony\Component\DependencyInjection\ContainerAwareInterface
{

    /** @var  \Symfony\Component\DependencyInjection\ContainerInterface */
    private $container;

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function __construct($container)
    {
        $this->setContainer($container);
    }


    public function postUpdate(\LovitBundle\Entity\ProjectGroup $group, \Doctrine\ORM\Event\LifecycleEventArgs $event)
    {

        $ids = array();
        /** @var User $user */
        foreach ($group->getUsers() as $user) {
            $gcmId = $user->getGcmRegistrationId();
            if (!empty($gcmId)) {
                $ids[] = $gcmId;
            }
        }
        if (count($ids)) {
            $this->sendMsg($ids, $group->getProject());
        }

        $this->container->get('logger')->log(Logger::DEBUG, 'GROUP UPDATED ' . $group->getId());
    }

    private function sendMsg(array $ids, Project $project)
    {
        // API access key from Google API's Console
        $key = 'AIzaSyCRFkYuLnzVcExjfvRC1x-ONnX5lCM6N9s';
        $registrationIds = $ids;

        $msg = array
        (
            'message'  => 'You\'ve been granted acces to project ' . $project->getName(),
            'title'    => 'Lavit Project Access',
            'subtitle' => 'New access granted',
//            'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
            'vibrate'  => 1,
            'sound'    => 1,
            'project'  => $project,
//            'largeIcon'  => 'large_icon',
//            'smallIcon'  => 'small_icon'
        );

        $fields = array
        (
            'registration_ids' => $registrationIds,
            'data'             => $msg
        );

        $headers = array
        (
            'Authorization: key=' . $key,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        $this->container->get('logger')->log(Logger::DEBUG, sprintf('SENT TO %s', json_encode($registrationIds)));
        $this->container->get('logger')->log(Logger::DEBUG, sprintf('Response %s', ($result)));
    }

}