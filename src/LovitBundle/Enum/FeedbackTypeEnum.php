<?php

namespace LovitBundle\Enum;

class FeedbackTypeEnum extends \Zantolov\AppBundle\Enum\BaseEnum
{

    const FEEDBACK_TYPE_RATING = 1;
    const FEEDBACK_TYPE_IMAGE = 2;
    const FEEDBACK_TYPE_LOCATION = 3;
    const FEEDBACK_TYPE_COMMENT = 4;


    protected static $labels = array(

        self::FEEDBACK_TYPE_RATING   => 'Rating',
        self::FEEDBACK_TYPE_IMAGE    => 'Image',
        self::FEEDBACK_TYPE_LOCATION => 'Location',
        self::FEEDBACK_TYPE_COMMENT  => 'Comment',
    );

}