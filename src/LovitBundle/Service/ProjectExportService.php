<?php

namespace LovitBundle\Service;

use Codsly\MyBeeLineAppBundle\Enum\ProductTypeEnum;
use LovitBundle\Entity\Feedback;
use LovitBundle\Entity\Project;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ProjectExportService implements ContainerAwareInterface
{

    /** @var  ContainerInterface */
    private $container;

    public function __construct($container = null)
    {
        $this->setContainer($container);
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    protected function getLabels()
    {
        return array(
            "id"        => 'ID',
            "project"   => "Project name",
            "rating"    => "Rating",
            "comment"   => "Comment",
            "latitude"  => 'Latitude',
            "longitude" => 'Longitude',
            "image"     => 'Image',
            "createdAt" => 'Created at',
            "updatedAt" => 'Updated at',
            "user"      => 'User',
        );
    }

    protected function prepareItem(Feedback $item)
    {
        $itemArray = $item->jsonSerialize();
        $user = (string)$item->getUser();
        $itemArray['user'] = $user;

        return $itemArray;
    }

    /**
     * @param $exampleListItem
     * @return array
     */
    public function prepareLabels($exampleListItem)
    {
        $keys = array_keys($exampleListItem);
        $defaultLabels = $this->getLabels();
        $labels = array();
        foreach ($keys as $i => $key) {
            $labels[$i] = $defaultLabels[$key];
        }
        return $labels;
    }


    public function getExportedResponse(Project $project)
    {
        $feedbacks = $project->getFeedbacks();

        $resultFeedbacks = array();

        if (!empty($feedbacks)) {
            foreach ($feedbacks as &$feedback) {
                $resultFeedbacks[] = $this->prepareItem($feedback);
            }
            
            if (empty($resultFeedbacks)) {
                return new Response('No data');
            }

            // Add labels
            array_unshift($resultFeedbacks, $this->prepareLabels($resultFeedbacks[0]));
        }

        /** @var \PHPExcel $phpExcelObject */
        $phpExcelObject = $this->container->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getActiveSheet()->fromArray($resultFeedbacks);

        $writer = $this->container->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');

        $response = $this->container->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'project.xls'
        );

        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

}