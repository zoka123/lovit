<?php


namespace LovitBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DataService implements ContainerAwareInterface
{

    /** @var  ContainerInterface $container */
    protected $container;


    public function __construct($container = null)
    {
        $this->setContainer($container);
    }


    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getGroupsForUser(User $user)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getEntityManager();

        $stmt = $em->getConnection()->prepare('SELECT project_group_id FROM projectgroup_users WHERE user_id = :user');
        $stmt->bindValue('user', $user->getId());
        $stmt->execute();
        $ids = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        $groups = $this->container->get('doctrine')->getManager()->getRepository('LovitBundle:ProjectGroup')
            ->findBy(array(
                'id'     => $ids,
                'active' => true,
            ));

        return $groups;
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getProjectsForUser(User $user)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getEntityManager();

        $stmt = $em->getConnection()->prepare('SELECT project_group_id FROM projectgroup_users WHERE user_id = :user');
        $stmt->bindValue('user', $user->getId());
        $stmt->execute();
        $ids = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        $qb = $em->getRepository('LovitBundle:ProjectGroup')->createQueryBuilder('G')
            ->resetDQLPart('select')
            ->select('P')
            ->innerJoin('LovitBundle:Project', 'P', 'WITH', 'G.project = P.id')
            ->where('G.id IN (:ids)')->setParameter('ids', $ids)
            ->andWhere('G.active = true')
            ->andWhere('P.active = true')
            ->getQuery();


        $groups = $qb->getResult();

        return $groups;
    }


    public function getSummaryForProject($projectId)
    {
        $sql = 'SELECT rating, COUNT(*) as count FROM `feedbacks` WHERE project_id = :projectId GROUP BY rating';

        $result = array();
        $sum = 0;
        $count = 0;

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('projectId', $projectId);
        $stmt->execute();
        $res = $stmt->fetchAll();

        foreach ($res as $item) {
            if (!isset($result[$item['rating']])) {
                $result[$item['rating']] = 0;
            }
            $result[$item['rating']] += $item['count'];
            $count += $item['count'];
            $sum += $item['rating'] * $item['count'];
        }

        if ($count > 0) {
            $result['avg'] = number_format($sum / $count, 2);
        } else {
            $result['avg'] = 0;
        }
        return $result;
    }

}
