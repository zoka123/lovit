<?php

namespace LovitBundle\DataFixtures\ORM;

use LovitBundle\Entity\Project;
use LovitBundle\Entity\Tag;
use Zantolov\AppBundle\DataFixtures\ORM\AbstractDbFixture;

class LoadTagsData extends AbstractDbFixture
{

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 10; $i++) {

            $item = new Tag();
            $item->setActive(true);
            $item->setName($faker->word);
            $this->addReference('tag' . $i, $item);
            $manager->persist($item);
        }

        for ($i = 1; $i < 50; $i++) {

            /** @var Project $project */
            $project = $this->getReference('project' . $i);
            $manager->persist($project);

            for ($j = 1; $j < 10; $j++) {
                if ((rand(1, 10) % 3) == 0) {
                    $project->addTag($this->getReference('tag' . $j));
                }
            }
        }


        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

}
