<?php

namespace LovitBundle\DataFixtures\ORM;

use LovitBundle\Entity\ProjectGroup;
use Zantolov\AppBundle\DataFixtures\ORM\AbstractDbFixture;

use Zantolov\AppBundle\Entity\User;

class LoadGroupsData extends AbstractDbFixture
{

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 15; $i++) {
            $group = new ProjectGroup();
            $group->setName($faker->word);
            $group->setActive(true);
            $group->setProject($this->getReference('project' . rand(1, 49)));
            $manager->persist($group);

            for ($j = 1; $j < 20; $j++) {
                $user = $this->getReference('user' . rand(1, 49));
                if(!$group->getUsers()->contains($user)){
                    $group->addUser($user);
                }
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

}
