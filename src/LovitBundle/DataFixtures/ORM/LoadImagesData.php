<?php

namespace LovitBundle\DataFixtures\ORM;

use Zantolov\AppBundle\DataFixtures\ORM\AbstractDbFixture;
use Zantolov\MediaBundle\Entity\Image;

class LoadImagesData extends AbstractDbFixture
{

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        $img = new Image();
        $img->setActive(true);
        $img->setImageName('default.jpg');

        $url = 'http://lorempixel.com/500/500/';
        $src = realpath(__DIR__ . '/../../../../web/media') . '/hq-background-01.jpg';
        $imgFile = realpath(__DIR__ . '/../../../../web/uploads/images/default') . '/default.jpg';
        file_put_contents($imgFile, file_get_contents($url));

        $this->setReference('imageDefault', $img);

        $manager->persist($img);

        $manager->flush();
    }

}
