<?php

namespace LovitBundle\DataFixtures\ORM;

use Doctrine\Common\Collections\ArrayCollection;
use LovitBundle\Entity\Project;
use LovitBundle\Enum\FeedbackTypeEnum;
use Zantolov\AppBundle\DataFixtures\ORM\AbstractDbFixture;

use Zantolov\AppBundle\Entity\User;

class LoadProjectsData extends AbstractDbFixture
{

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 50; $i++) {

            $item = new Project();
            $item->setActive(true);
            $item->setAllowedTypes(array(1, 2, 3, 4));
            $item->setImage($this->getReference('image' . rand(0, 19)));
            $item->setName($faker->sentence());
            $item->setDescription($faker->realText());
            $this->addReference('project' . $i, $item);
            $manager->persist($item);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }

}
