<?php

namespace LovitBundle\DataFixtures\ORM;

use LovitBundle\Entity\Feedback;
use LovitBundle\Entity\Project;
use Zantolov\AppBundle\DataFixtures\ORM\AbstractDbFixture;

use Zantolov\AppBundle\Entity\User;

class LoadFeedbackData extends AbstractDbFixture
{

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 200; $i++) {

            $item = new Feedback();
            $item->setComment($faker->sentence());
            $item->setImagePath($faker->word);
            $item->setLatitude($faker->latitude);
            $item->setLongitude($faker->longitude);
            $item->setRating(rand(1, 5));
            $item->setProject($this->getReference('project' . rand(1, 49)));
            $item->setUser($this->getReference('user' . rand(1, 49)));
            $this->addReference('feedback' . $i, $item);
            $manager->persist($item);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

}
