<?php

namespace LovitBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Zantolov\AppBundle\Entity\Interfaces\SluggableInterface;
use Zantolov\AppBundle\Entity\Traits\ActivableTrait;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Zantolov\AppBundle\Entity\Traits\SluggableTrait;

/**
 * @ORM\Entity ()
 * @ORM\Table(name="tags")
 * @ORM\HasLifecycleCallbacks
 */
class Tag implements SluggableInterface, \JsonSerializable
{
    use ActivableTrait;
    use TimestampableEntity;
    use SluggableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @var ArrayCollection $projects
     * @ORM\ManyToMany(targetEntity="Project", mappedBy="tags")
     **/
    private $projects;


    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function getSluggableProperty()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * @param Project $project
     * @return Tag
     */
    public function addProduct(Project $project)
    {
        $this->projects->add($project);

        return $this;
    }

    /**
     *
     * @param Project $project
     */
    public function removeProduct(Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name
        );
    }


}