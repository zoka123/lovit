<?php

namespace LovitBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Zantolov\MediaBundle\Entity\Traits\ImageableTrait;


/**
 * @ORM\Entity
 * @ORM\Table(name="projects")
 * @ORM\HasLifecycleCallbacks
 */
class Project implements \JsonSerializable
{
    use \Gedmo\Timestampable\Traits\TimestampableEntity;
    use \Zantolov\AppBundle\Entity\Traits\ActivableTrait;
    use ImageableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @var ArrayCollection $tags
     *
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinTable(name="project_tags")
     **/
    protected $tags;


    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Feedback", mappedBy="project")
     */
    private $feedbacks;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ProjectGroup", mappedBy="project")
     */
    private $groups;


    /**
     * @var ArrayCollection
     * @ORM\Column(type="array", nullable=true)
     */
    private $allowedTypes = array();

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->feedbacks = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return array(
            'id'          => $this->getId(),
            'name'        => $this->getName(),
            'description' => $this->getDescription(),
            'active'      => $this->getActive(),
            'createdAt'   => $this->getCreatedAt()->format(DATE_ATOM),
            'updatedAt'   => $this->getUpdatedAt()->format(DATE_ATOM),
            'tags'        => $this->getTags()->toArray(),
            'allowedTypes'        => $this->getAllowedTypes(),
        );
    }


    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Tag $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @param $tag
     */
    public function addTag($tag)
    {
        $this->getTags()->add($tag);
    }

    /**
     * @param $tag
     */
    public function removeTag($tag)
    {
        $this->getTags()->remove($tag);
    }

    /**
     * @return mixed
     */
    public function getFeedbacks()
    {
        return $this->feedbacks;
    }

    /**
     * @param mixed $feedbacks
     */
    public function addFeedback($feedback)
    {
        $this->feedbacks->add($feedback);
    }


    /**
     * @param mixed $feedbacks
     */
    public function removeFeedback($feedback)
    {
        $this->feedbacks->remove($feedback);
    }

    /**
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return ArrayCollection
     */
    public function getAllowedTypes()
    {
        return $this->allowedTypes;
    }

    /**
     * @param ArrayCollection $allowedTypes
     */
    public function setAllowedTypes($allowedTypes)
    {
        $this->allowedTypes = $allowedTypes;
    }

}
