<?php

namespace LovitBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\EntityListeners({ "LovitBundle\EventListener\GroupListener" })
 * @ORM\Table(name="projectgroups")
 * @ORM\HasLifecycleCallbacks
 */
class ProjectGroup implements \JsonSerializable
{
    use \Gedmo\Timestampable\Traits\TimestampableEntity;
    use \Zantolov\AppBundle\Entity\Traits\ActivableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @var ArrayCollection $users
     *
     * @ORM\ManyToMany(targetEntity="Zantolov\AppBundle\Entity\User")
     * @ORM\JoinTable(name="projectgroup_users",
     *      joinColumns={@ORM\JoinColumn(name="project_group_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     **/
    protected $users;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="groups")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $project;


    public function __construct()
    {
        $this->users = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return array(
            'id'        => $this->getId(),
            'name'      => $this->getName(),
            'active'    => $this->getActive(),
            'createdAt' => $this->getCreatedAt()->format(DATE_ATOM),
            'updatedAt' => $this->getUpdatedAt()->format(DATE_ATOM),
        );
    }


    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function addUser($user)
    {
        $this->users->add($user);
    }

    /**
     * @param ArrayCollection $users
     */
    public function removeUser($user)
    {
        $this->users->remove($user);
    }


    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param mixed $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

}
