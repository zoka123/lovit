<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24.09.15.
 * Time: 22:42
 */

namespace LovitBundle\Controller;

use LovitBundle\Entity\Project;
use LovitBundle\Enum\FeedbackTypeEnum;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Zantolov\AppBundle\Controller\EntityCrudController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LovitBundle\Entity\Feedback;
use LovitBundle\Form\FeedbackType;
use Zantolov\AppBundle\Entity\ApiToken;
use Zantolov\AppBundle\Entity\User;

/**
 * MobileFeedback controller.
 *
 * @Route("/mobile/feedback")
 */
class MobileFeedbackController extends EntityCrudController
{

    protected function getToken()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $token = $this->get('session')->get('userToken');
        if (empty($token) || !($token instanceof TokenStorageInterface)) {
            /** @var User $user */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $token = new ApiToken();
            $token->setUser($user);
            $token->setToken(sha1(uniqid()));
            $token->setUserAgent($this->getRequest()->headers->get('User-Agent'));
            $this->getDoctrine()->getManager()->persist($token);
            $this->getDoctrine()->getManager()->flush();
            $this->get('session')->set('userToken', $token->getToken());
        }
        return $token->getToken();
    }

    protected function getEntityClass()
    {
        return 'LovitBundle:Feedback';
    }

    /**
     * @param $project
     * @return array|bool
     */
    private function checkProject($project)
    {
        /** @var Project $project */
        if (empty($project)) {
            return $this->createErrorResponse('Project not found');
        }

        if ($project->getActive() != true) {
            return $this->createErrorResponse('Project not active');
        }

        return true;
    }

    /**
     * Lists all Feedback entities.
     *
     * @Route("/{projectId}", name="mobile.feedback.index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $projectId)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        /** @var Project $project */
        $project = $this->getDoctrine()->getRepository('LovitBundle:Project')->find($projectId);

        $check = $this->checkProject($project);

        if ($check != true) {
            return $check;
        }

        $token = $this->getToken();

        return $this->render(
            'LovitBundle:MobileFeedback:index.html.twig', ['project' => $project, 'token' => $token]
        );
    }

    protected function createCreateForm($entity)
    {
        // TODO: Implement createCreateForm() method.
    }

    protected function createDeleteForm($id)
    {
        // TODO: Implement createDeleteForm() method.
    }

    protected function createEditForm($entity)
    {
        // TODO: Implement createEditForm() method.
    }
}