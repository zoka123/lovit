<?php

namespace LovitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Zantolov\AppBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction()
    {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($user instanceof User) {
            if ($user->hasRole('ROLE_ADMIN')) {
                return $this->redirectToRoute('project.index');
            } elseif ($user->hasRole('ROLE_USER')) {
                return $this->redirectToRoute('mobile.projects.index');
            }
        } else {
            return $this->redirectToRoute('public.project.index');

        }

        return array();
    }

    /**
     * @Route("/app/", name="homepage.app")
     */
    public function appAction()
    {
        return $this->indexAction();
    }
}
