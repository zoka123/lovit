<?php

namespace LovitBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Zantolov\AppBundle\Controller\EntityCrudController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LovitBundle\Entity\Project;
use LovitBundle\Form\ProjectType;

/**
 * PublicProject controller.
 *
 * @Route("/public/projects")
 */
class PublicProjectController extends EntityCrudController
{

    protected function getEntityClass()
    {
        return 'LovitBundle:Project';
    }

    /**
     * Lists all Projects.
     *
     * @Route("/", name="public.project.index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $projects = $this->getDoctrine()->getRepository('LovitBundle:Project')->findAll();

        return $this->render(
            'LovitBundle:PublicProject:index.html.twig',
            array('projects' => $projects)
        );
    }

    /**
     * Display project details.
     *
     * @Route("/{id}", name="getPublicProjectDetails")
     * @Method("GET")
     * @Template()
     */
    public function getProjectDetailsAction($id)
    {
        $project = $this->getDoctrine()->getRepository('LovitBundle:Project')->find($id);

        return $this->render(
            'LovitBundle:PublicProject:getProjectDetails.html.twig',
            array('project' => $project)
        );
    }

    /**
     * Send email
     *
     * @Route("/{id}", name="publicProjectEmail.send")
     * @Method("POST")
     * @Template()
     */
    public function sendEmailAction(Request $request, $id)
    {
        $email = $request->get('email');

        $project = $this->getDoctrine()->getRepository('LovitBundle:Project')->find($id);

        $message = \Swift_Message::newInstance()
            ->setSubject('Hello Email')
            ->setFrom($email)
            ->setTo('admin@lavit.com')
            ->setBody(
                'User with email: ' . $email . ' wants to give feedback on project: ' . $project->getName()
            )
        ;

        $this->get('mailer')->send($message);

        return new JsonResponse(
            array(
                "status" => "ok",
                "message" => 'Subscribed!',
            ));
    }


    protected function createCreateForm($entity)
    {
        // TODO: Implement createCreateForm() method.
    }

    protected function createDeleteForm($id)
    {
        // TODO: Implement createDeleteForm() method.
    }

    protected function createEditForm($entity)
    {
        // TODO: Implement createEditForm() method.
    }
}
