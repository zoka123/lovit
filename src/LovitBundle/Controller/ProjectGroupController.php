<?php

namespace LovitBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Zantolov\AppBundle\Controller\EntityCrudController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LovitBundle\Entity\ProjectGroup;
use LovitBundle\Form\ProjectGroupType;

/**
 * ProjectGroup controller.
 *
 * @Route("/app/admin/groups")
 */
class ProjectGroupController extends EntityCrudController
{

    protected function getEntityClass()
    {
        return 'LovitBundle:ProjectGroup';
    }

    /**
     * Lists all ProjectGroup entities.
     *
     * @Route("/", name="groups.index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        return parent::baseIndexAction($request);
    }

    /**
     * Creates a new ProjectGroup entity.
     *
     * @Route("/", name="groups.create")
     * @Method("POST")
     * @Template("LovitBundle:ProjectGroup:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $group = new ProjectGroup();
        return parent::baseCreateAction(
            $request,
            $group,
            'groups.show');


    }

    /**
     * Creates a form to create a ProjectGroup entity.
     *
     * @param ProjectGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($entity)
    {
        return parent::createBaseCreateForm(
            $entity,
            new ProjectGroupType(),
            $this->generateUrl('groups.create')
        );
    }

    /**
     * Displays a form to create a new ProjectGroup entity.
     *
     * @Route("/new", name="groups.new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $group = new ProjectGroup();
        $project = $request->get('project');
        if (!empty($project)) {
            $projectObj = $this->getManager()->getRepository('LovitBundle:Project')->find($project);
            if (!empty($projectObj)) {
                $group->setProject($projectObj);
            }
        }

        return parent::baseNewAction($group);
    }

    /**
     * Finds and displays a ProjectGroup entity.
     *
     * @Route("/{id}", name="groups.show", requirements={"id"="\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        return parent::baseShowAction($id);
    }

    /**
     * Displays a form to edit an existing ProjectGroup entity.
     *
     * @Route("/{id}/edit", name="groups.edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {

        return parent::baseEditAction($id);
    }

    /**
     * Creates a form to edit a ProjectGroup entity.
     *
     * @param ProjectGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($entity)
    {
        return parent::createBaseEditForm(
            $entity,
            new ProjectGroupType(),
            $this->generateUrl('groups.update', array('id' => $entity->getId()))
        );
    }

    /**
     * Edits an existing ProjectGroup entity.
     *
     * @Route("/{id}", name="groups.update")
     * @Method("PUT")
     * @Template("LovitBundle:ProjectGroup:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {

        return parent::baseUpdateAction(
            $request,
            $id,
            $this->generateUrl('groups.edit', array('id' => $id))
        );
    }

    /**
     * Deletes a ProjectGroup entity.
     *
     * @Route("/{id}", name="groups.delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {

        return parent::baseDeleteAction(
            $request,
            $id,
            $this->generateUrl('groups.index')
        );
    }

    /**
     * Creates a form to delete a ProjectGroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($id)
    {
        return parent::baseCreateDeleteForm(
            $this->generateUrl('groups.delete', array('id' => $id))
        );
    }


    /**
     * @param $userId
     * @Template()
     */
    public function getGroupsForUserAction($userId)
    {
        $user = $this->getDoctrine()->getManager()->getRepository('ZantolovAppBundle:User')->find($userId);
        if (empty($user)) {
            return array();
        }

        $groups = $this->get('lovit.data')->getGroupsForUser($user);

        return compact('groups');

    }


    /**
     * @param $userId
     * @Template()
     */
    public function getProjectsForUserAction($userId)
    {
        $user = $this->getDoctrine()->getManager()->getRepository('ZantolovAppBundle:User')->find($userId);
        if (empty($user)) {
            return array();
        }

        $projects = $this->get('lovit.data')->getProjectsForUser($user);

        return compact('projects');
    }
}
