<?php

namespace LovitBundle\Controller;

use Zantolov\AppBundle\Controller\EntityCrudController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LovitBundle\Entity\Feedback;
use LovitBundle\Form\FeedbackType;

/**
 * Feedback controller.
 *
 * @Route("/app/admin/feedback")
 */
class FeedbackController extends EntityCrudController
{

    protected function getEntityClass()
    {
        return 'LovitBundle:Feedback';
    }

    /**
     * Lists all Feedback entities.
     *
     * @Route("/", name="feedback.index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        return parent::baseIndexAction($request);
    }

    /**
     * Creates a new Feedback entity.
     *
     * @Route("/", name="feedback.create")
     * @Method("POST")
     * @Template("LovitBundle:Feedback:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::baseCreateAction(
            $request,
            new Feedback(),
            'feedback.show');


    }

    /**
     * Creates a form to create a Feedback entity.
     *
     * @param Feedback $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($entity)
    {
        return parent::createBaseCreateForm(
            $entity,
            new FeedbackType(),
            $this->generateUrl('feedback.create')
        );
    }

    /**
     * Displays a form to create a new Feedback entity.
     *
     * @Route("/new", name="feedback.new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {

        return parent::baseNewAction(new Feedback());
    }

    /**
     * Finds and displays a Feedback entity.
     *
     * @Route("/{id}", name="feedback.show", requirements={"id"="\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        return parent::baseShowAction($id);
    }

    /**
     * Displays a form to edit an existing Feedback entity.
     *
     * @Route("/{id}/edit", name="feedback.edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {

        return parent::baseEditAction($id);
    }

    /**
     * Creates a form to edit a Feedback entity.
     *
     * @param Feedback $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($entity)
    {
        return parent::createBaseEditForm(
            $entity,
            new FeedbackType(),
            $this->generateUrl('feedback.update', array('id' => $entity->getId()))
        );
    }

    /**
     * Edits an existing Feedback entity.
     *
     * @Route("/{id}", name="feedback.update")
     * @Method("PUT")
     * @Template("LovitBundle:Feedback:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {

        return parent::baseUpdateAction(
            $request,
            $id,
            $this->generateUrl('feedback.edit', array('id' => $id))
        );
    }

    /**
     * Deletes a Feedback entity.
     *
     * @Route("/{id}", name="feedback.delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {

        return parent::baseDeleteAction(
            $request,
            $id,
            $this->generateUrl('feedback.index')
        );
    }

    /**
     * Creates a form to delete a Feedback entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($id)
    {
        return parent::baseCreateDeleteForm(
            $this->generateUrl('feedback.delete', array('id' => $id))
        );
    }
}
