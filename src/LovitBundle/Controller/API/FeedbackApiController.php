<?php

namespace LovitBundle\Controller\API;

use Doctrine\Common\Util\Debug;
use LovitBundle\Entity\Feedback;
use LovitBundle\Entity\Project;
use Monolog\Logger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Zantolov\AppBundle\Controller\API\ApiController;
use Zantolov\MediaBundle\Entity\Image;

/**
 * @Route("/app/api/feedbacks")
 */
class FeedbackApiController extends ApiController
{

    /**
     * @return mixed
     */
    protected function getCurrentUser()
    {
        return $this->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @param $project
     * @return array|bool
     */
    private function checkProject($project)
    {
        /** @var Project $project */
        if (empty($project)) {
            return $this->createErrorResponse('Project not found');
        }

        if ($project->getActive() != true) {
            return $this->createErrorResponse('Project not active');
        }

        return true;
    }

    /**
     * @Route("/{projectId}", name="api.feedbacks.get")
     * @Method("GET")
     */
    public function getAction(Request $request, $projectId)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        /** @var Project $project */
        $project = $this->getDoctrine()->getRepository('LovitBundle:Project')->find($projectId);

        $check = $this->checkProject($project);
        if ($check != true) {
            return $check;
        }

        $feedback = $project->getFeedbacks()->toArray();

        return $this->createResponse(
            array(
                self::KEY_STATUS => self::STATUS_OK,
                self::KEY_DATA   => $feedback,
            ));
    }


    /**
     * @Route("/{projectId}", name="api.feedbacks.post")
     * @Method("POST")
     */
    public function postAction(Request $request, $projectId)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getCurrentUser();

        /** @var Project $project */
        $project = $this->getDoctrine()->getRepository('LovitBundle:Project')->find($projectId);

        $check = $this->checkProject($project);
        if ($check !== true) {
            return $check;
        }

        $data = $this->getDataFromRequest($request);

        $feedback = $this->getDoctrine()->getManager()->getRepository('LovitBundle:Feedback')->findOneBy(
            array(
                'project' => $projectId,
                'user'    => $user->getId(),
            )
        );
        if (empty($feedback)) {
            $feedback = new Feedback();
        }

        $feedback->setUser($user);
        $feedback->setProject($project);

        if (!isset($data['rating']) || empty($data['rating'])) {
            return $this->createErrorResponse('Rating not set');
        }

        $feedback->setRating($data['rating']);

        if (isset($data['comment'])) {
            $feedback->setComment($data['comment']);
        }

        if (isset($data['imagePath'])) {
            $feedback->setImagePath($data['imagePath']);

            $image = $this->getDoctrine()->getManager()->getRepository('ZantolovMediaBundle:Image')->findOneBy(
                array('imageName' => $feedback->getImagePath()));

            if (!empty($image)) {
                $feedback->setImage($image);
            }
        }

        if (isset($data['latitude'])) {
            $feedback->setLatitude(floatval($data['latitude']));
        }

        if (isset($data['longitude'])) {
            $feedback->setLongitude(floatval($data['longitude']));
        }

        $this->getDoctrine()->getManager()->persist($feedback);
        $this->getDoctrine()->getManager()->flush();

        return $this->createResponse(
            array(
                self::KEY_STATUS  => self::STATUS_OK,
                self::KEY_MESSAGE => 'Created feedback',
            ));
    }

    /**
     * @Route("/{projectId}/image", name="api.feedbacks.postImage")
     */
    public function postFeedbackImageAction(Request $request, $projectId)
    {
        /** @var Logger $logger */
        $logger = $this->get('logger');

        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('avatar');
            $name = time() . '_' . $file->getClientOriginalName() . '.' . $file->guessClientExtension();

            $img = new Image();
            $img->setActive(true);
            $img->setImageName($name);
            $imgFilePath = realpath(__DIR__ . '/../../../../web/uploads/images/default');

            $file->move($imgFilePath, $name);

            $this->getDoctrine()->getManager()->persist($img);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array(
                'data' => $img->getImageName()
            ));

        } catch (\Exception $e) {
            $logger->log(Logger::ERROR, $e->getMessage(), $e->getTrace());
            return new JsonResponse(array(
                'data' => 'fail',
            ));
        }
    }
}
