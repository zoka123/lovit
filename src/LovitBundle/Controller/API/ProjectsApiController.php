<?php

namespace LovitBundle\Controller\API;

use Doctrine\Common\Util\Debug;
use LovitBundle\Entity\Feedback;
use LovitBundle\Entity\Project;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Zantolov\AppBundle\Controller\API\ApiController;

/**
 * @Route("/app/api/projects")
 */
class ProjectsApiController extends ApiController
{

    /**
     * @param $project
     * @return array|bool
     */
    private function checkProject($project)
    {
        /** @var Project $project */
        if (empty($project)) {
            return $this->createErrorResponse('Project not found');
        }

        if ($project->getActive() != true) {
            return $this->createErrorResponse('Project not active');
        }

        return true;
    }

    /**
     * @return mixed
     */
    protected function getCurrentUser()
    {
        return $this->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @Route("/", name="api.projects.get")
     * @Method("GET")
     */
    public function getProjectsAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getCurrentUser();

        $projects = $this->get('lovit.data')->getProjectsForUser($user);

        return $this->createResponse(
            array(
                self::KEY_STATUS => self::STATUS_OK,
                self::KEY_DATA   => $projects,
            ));
    }

    /**
     * @Route("/{projectId}", name="api.projects.getMy")
     * @Method("GET")
     */
    public function getDetailsProjectsAction(Request $request, $projectId)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getCurrentUser();

        /** @var Project $project */
        $project = $this->getDoctrine()->getRepository('LovitBundle:Project')->find($projectId);

        $check = $this->checkProject($project);
        if ($check != true) {
            return $check;
        }

        /** @var Feedback $feedback */
        $feedback = $this->getDoctrine()->getManager()->getRepository('LovitBundle:Feedback')
            ->findBy(array(
                'project' => $project,
                'user'    => $this->getCurrentUser(),
            ));

        $project = $project->jsonSerialize();
        $summary = $this->get('lovit.data')->getSummaryForProject($projectId);
        $project['avg'] = $summary['avg'];

        return $this->createResponse(
            array(
                self::KEY_STATUS => self::STATUS_OK,
                self::KEY_DATA   => array('project' => $project, 'feedback' => $feedback),
            ));
    }

}