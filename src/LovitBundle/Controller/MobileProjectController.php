<?php

namespace LovitBundle\Controller;

use Zantolov\AppBundle\Controller\EntityCrudController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LovitBundle\Entity\Feedback;
use LovitBundle\Form\FeedbackType;

/**
 * MobileProject controller.
 *
 * @Route("/mobile/projects")
 */
class MobileProjectController extends EntityCrudController
{

    protected function getEntityClass()
    {
        return 'LovitBundle:Project';
    }

    /**
     * @return mixed
     */
    protected function getCurrentUser()
    {
        return $this->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * Lists all Feedback entities.
     *
     * @Route("/", name="mobile.projects.index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        // @TODO checkirati dal je logirani?
        $user = $this->getCurrentUser();

        $projects = $this->get('lovit.data')->getProjectsForUser($user);

        return $this->render(
          'LovitBundle:MobileProject:index.html.twig',
           array('projects' => $projects)
        );
    }

    protected function createCreateForm($entity)
    {
        //
    }

    protected function createDeleteForm($id)
    {
        //
    }

    protected function createEditForm($entity)
    {
        //
    }
}
