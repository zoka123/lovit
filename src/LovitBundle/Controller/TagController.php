<?php

namespace LovitBundle\Controller;

use Zantolov\AppBundle\Controller\EntityCrudController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LovitBundle\Entity\Tag;
use LovitBundle\Form\TagType;

/**
 * Tag controller.
 *
 * @Route("/app/admin/tags")
 */
class TagController extends EntityCrudController
{

    protected function getEntityClass()
    {
        return 'LovitBundle:Tag';
    }

    /**
     * Lists all Tag entities.
     *
     * @Route("/", name="lovit.tags.index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        return parent::baseIndexAction($request);
    }

    /**
     * Creates a new Tag entity.
     *
     * @Route("/", name="lovit.tags.create")
     * @Method("POST")
     * @Template("LovitBundle:Tag:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::baseCreateAction(
            $request,
            new Tag(),
            'lovit.tags.show');


    }

    /**
     * Creates a form to create a Tag entity.
     *
     * @param Tag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($entity)
    {
        return parent::createBaseCreateForm(
            $entity,
            new TagType(),
            $this->generateUrl('lovit.tags.create')
        );
    }

    /**
     * Displays a form to create a new Tag entity.
     *
     * @Route("/new", name="lovit.tags.new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {

        return parent::baseNewAction(new Tag());
    }

    /**
     * Finds and displays a Tag entity.
     *
     * @Route("/{id}", name="lovit.tags.show", requirements={"id"="\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        return parent::baseShowAction($id);
    }

    /**
     * Displays a form to edit an existing Tag entity.
     *
     * @Route("/{id}/edit", name="lovit.tags.edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {

        return parent::baseEditAction($id);
    }

    /**
     * Creates a form to edit a Tag entity.
     *
     * @param Tag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($entity)
    {
        return parent::createBaseEditForm(
            $entity,
            new TagType(),
            $this->generateUrl('lovit.tags.update', array('id' => $entity->getId()))
        );
    }

    /**
     * Edits an existing Tag entity.
     *
     * @Route("/{id}", name="lovit.tags.update")
     * @Method("PUT")
     * @Template("LovitBundle:Tag:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {

        return parent::baseUpdateAction(
            $request,
            $id,
            $this->generateUrl('lovit.tags.edit', array('id' => $id))
        );
    }

    /**
     * Deletes a Tag entity.
     *
     * @Route("/{id}", name="lovit.tags.delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {

        return parent::baseDeleteAction(
            $request,
            $id,
            $this->generateUrl('lovit.tags.index')
        );
    }

    /**
     * Creates a form to delete a Tag entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($id)
    {
        return parent::baseCreateDeleteForm(
            $this->generateUrl('lovit.tags.delete', array('id' => $id))
        );
    }
}
