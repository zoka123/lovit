<?php

namespace LovitBundle\Controller;

use LovitBundle\Service\ProjectExportService;
use Symfony\Component\HttpFoundation\Response;
use Zantolov\AppBundle\Controller\EntityCrudController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use LovitBundle\Entity\Project;
use LovitBundle\Form\ProjectType;
use Zantolov\MediaBundle\Entity\Image;

/**
 * Project controller.
 *
 * @Route("/app/admin/projects")
 */
class ProjectController extends EntityCrudController
{

    protected function getEntityClass()
    {
        return 'LovitBundle:Project';
    }

    /**
     * Lists all Project entities.
     *
     * @Route("/", name="project.index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        return parent::baseIndexAction($request);
    }

    /**
     * Creates a new Project entity.
     *
     * @Route("/", name="project.create")
     * @Method("POST")
     * @Template("LovitBundle:Project:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::baseCreateAction(
            $request,
            new Project(),
            'project.show');


    }

    /**
     * Creates a form to create a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($entity)
    {
        return parent::createBaseCreateForm(
            $entity,
            new ProjectType(),
            $this->generateUrl('project.create')
        );
    }

    /**
     * Displays a form to create a new Project entity.
     *
     * @Route("/new", name="project.new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {

        return parent::baseNewAction(new Project());
    }

    /**
     * Finds and displays a Project entity.
     *
     * @Route("/{id}", name="project.show", requirements={"id"="\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        $showData = parent::baseShowAction($id);

        $summary = $this->get('lovit.data')->getSummaryForProject($id);
        $chart = array();
        foreach ($summary as $k => $item) {
            if ($k == 'avg') {
                continue;
            }
            $chart[] = array(sprintf('%s (%sx)', $k, $item), $item);

        }

        return array_merge($showData, compact('summary', 'chart'));

    }

    /**
     * Displays a form to edit an existing Project entity.
     *
     * @Route("/{id}/edit", name="project.edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {

        return parent::baseEditAction($id);
    }

    /**
     * Creates a form to edit a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($entity)
    {
        return parent::createBaseEditForm(
            $entity,
            new ProjectType(),
            $this->generateUrl('project.update', array('id' => $entity->getId()))
        );
    }

    /**
     * Edits an existing Project entity.
     *
     * @Route("/{id}", name="project.update")
     * @Method("PUT")
     * @Template("LovitBundle:Project:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {

        return parent::baseUpdateAction(
            $request,
            $id,
            $this->generateUrl('project.edit', array('id' => $id))
        );
    }

    /**
     * Deletes a Project entity.
     *
     * @Route("/{id}", name="project.delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {

        return parent::baseDeleteAction(
            $request,
            $id,
            $this->generateUrl('project')
        );
    }

    /**
     * Creates a form to delete a Project entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($id)
    {
        return parent::baseCreateDeleteForm(
            $this->generateUrl('project.delete', array('id' => $id))
        );
    }

    /**
     * @Route("/{id}/export", name="project.export", requirements={"id"="\d+"})
     * @Method("GET")
     * @Template()
     */
    public function exportAction($id)
    {
        $entity = $this->getEntityById($id);

        /** @var ProjectExportService $exporter */
        $exporter = $this->get('export.project');
        return $exporter->getExportedResponse($entity);
    }

    /**
     * @Route("/{id}/image", name="project.image", requirements={"id"="\d+"})
     * @Method("GET")
     */
    public function projectImageAction($id)
    {

        /** @var Project $entity */
        $entity = $this->getEntityById($id);

        $image = $entity->getImage();
        if ($image instanceof Image) {

        } else {
            $image = $this->getManager()->getRepository('ZantolovMediaBundle:Image')->findOneBy(
                array('imageName' => 'default.jpg')
            );
        }

        if (empty($image)) {
            return new Response('No image');
        }

        $url = $this->get('zantolov.media.vichuploader')->getAssetUrl($image, 'imageFile', null, false);
        $url = $this->get('liip_imagine.cache.manager')->getBrowserPath($url, '500x330');
        return $this->redirect($url);


    }
}
